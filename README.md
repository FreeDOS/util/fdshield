FDSHIELD virus shield - by Eric Auer 2004-2006 eric _AT_ coli.uni-sb.de
To compile: http://nasm.sf.net/ "nasm -o fdshield.com fdshield.asm"

FDSHIELD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

FDSHIELD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FDSHIELD; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
(or try http://www.gnu.org/licenses/licenses.html at www.gnu.org).
