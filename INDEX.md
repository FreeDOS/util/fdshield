# FDSHIELD

FDSHIELD virus shield - by Eric Auer 2004-2006 eric _AT_ coli.uni-sb.de
To compile: http://nasm.sf.net/ "nasm -o fdshield.com fdshield.asm"

FDSHIELD is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

FDSHIELD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FDSHIELD; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
(or try http://www.gnu.org/licenses/licenses.html at www.gnu.org).

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FDSHIELD.LSM

<table>
<tr><td>title</td><td>FDSHIELD</td></tr>
<tr><td>version</td><td>06aug2006 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2016-05-19</td></tr>
<tr><td>description</td><td>Malware action blocker and warner</td></tr>
<tr><td>keywords</td><td>freedos, vsafe, virus, shield</td></tr>
<tr><td>author</td><td>Eric Auer eric -at- coli.uni-sb.de</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer eric -at- coli.uni-sb.de</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.auersoft.eu/soft/specials/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>Malware action blocker and warner, helps to reduce virus activity on your DOS</td></tr>
</table>
